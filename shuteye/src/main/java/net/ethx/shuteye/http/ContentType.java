package net.ethx.shuteye.http;

import net.ethx.shuteye.util.Preconditions;

/**
 * Micro-type around content types. Used to add some type safety to the various combinations of otherwise stringly-typed methods in
 * {@link net.ethx.shuteye.http.request.PostingRequest}.
 */
public class ContentType {
    private static final String WC = "*";

    public static final ContentType WILDCARD = ContentType.parse(WC);
    public static final ContentType APPLICATION_OCTET_STREAM = ContentType.create("application", "octet-stream");
    public static final ContentType MULTIPART_FORM_DATA = ContentType.create("multipart", "form-data");
    public static final ContentType TEXT_PLAIN = ContentType.create("text", "plain");

    private final String type;
    private final String subtype;

    ContentType(final String type, final String subtype) {
        Preconditions.checkArgument(!type.isEmpty() && !subtype.isEmpty(), "Invalid arguments type:'%s', subtype:'%s'", type, subtype);
        Preconditions.checkArgument(!WC.equals(type) || WC.equals(subtype), "Wildcard type provided, but non-wildcard subtype '%s' provided", subtype);

        this.type = type;
        this.subtype = subtype;
    }

    @Override
    public String toString() {
        return type + "/" + subtype;
    }

    /**
     * Creates a content type from the specified type and subtype. For example, "application/xml" would require
     * a type of "application", and a subtype of "xml"
     *
     * @param type    Major type
     * @param subtype Subtype
     * @return The created ContentType
     */
    public static ContentType create(final String type, final String subtype) {
        return new ContentType(type, subtype);
    }

    /**
     * Parses a content type from the specification string. Example content types might include "application/*", "application/octet-stream",
     * "text/json", etc.
     *
     * @param contentType Content type specification
     * @return The parsed content type
     */
    public static ContentType parse(final String contentType) {
        final int index = contentType.indexOf('/');
        final String type = index < 0 ? contentType : contentType.substring(0, index);
        final String subtype = index < 0 ? WC : contentType.substring(index + 1);
        return new ContentType(type, subtype);
    }

    /**
     * Checks if this content type is compatible with the other content type. <code>text/xml</code> is compatible with
     * <code>text/*</code>, for example, and <code>*</code> is compatible with anything.
     *
     * @param other Content type to match
     * @return true If the content types are compatible, false otherwise
     */
    public boolean isCompatibleWith(final ContentType other) {
        //  true if either side is wildcard at type level; no need to check subtypes per ctor precondition
        if (WC.equals(type) || WC.equals(other.type)) {
            return true;
        }

        //  if our types don't match, then we don't match.
        if (!type.equals(other.type)) {
            return false;
        }

        //  true if either side is wildcard at type level
        if (WC.equals(subtype) || WC.equals(other.subtype)) {
            return true;
        }

        //  final check
        return subtype.equals(other.subtype);
    }

    /**
     * @return The type of this content type; for example, <code>text/plain</code> has a type of <code>text</code>
     */
    public String getType() {
        return type;
    }

    /**
     * @return The subtype of this content type; for example, <code>text/plain</code> has a subtype of <code>plain</code>
     */
    public String getSubtype() {
        return subtype;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final ContentType that = (ContentType) o;
        return type.equals(that.type) && subtype.equals(that.subtype);
    }

    @Override
    public int hashCode() {
        return 31 * type.hashCode() + subtype.hashCode();
    }
}
