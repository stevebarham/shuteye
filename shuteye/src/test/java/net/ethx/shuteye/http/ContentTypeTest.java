package net.ethx.shuteye.http;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ContentTypeTest {
    @Test
    public void parse() {
        final ContentType parsed = ContentType.parse("text/plain");
        Assert.assertEquals(ContentType.TEXT_PLAIN, parsed);
        Assert.assertEquals("text", parsed.getType());
        Assert.assertEquals("plain", parsed.getSubtype());
    }

    @Test
    public void rejectOddWildcard() {
        expect(IllegalArgumentException.class, "*", "foo");
        expect(IllegalArgumentException.class, "", "foo");
        expect(IllegalArgumentException.class, "foo", "");
    }

    @Test
    public void isCompatibleWith() {
        assertTrue(ContentType.parse("*").isCompatibleWith(ContentType.TEXT_PLAIN));
        assertTrue(ContentType.parse("text/*").isCompatibleWith(ContentType.TEXT_PLAIN));
        assertTrue(ContentType.parse("application/xml").isCompatibleWith(ContentType.parse("application/*")));
        assertTrue(ContentType.parse("application/xml").isCompatibleWith(ContentType.parse("application/xml")));

        assertFalse(ContentType.parse("application/xml").isCompatibleWith(ContentType.parse("text/xml")));
        assertFalse(ContentType.parse("application/*").isCompatibleWith(ContentType.parse("text/xml")));
    }


    void expect(Class<? extends Throwable> throwableClass, final String type, final String subtype) {
        try {
            new ContentType(type, subtype);
            fail("Expected " + throwableClass);
        } catch (Exception e) {
            if (!throwableClass.isInstance(e)) {
                throw new RuntimeException(e);
            }
        }
    }
}