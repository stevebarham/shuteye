package net.ethx.shuteye.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class JoinerTest {
    @Test
    public void join() throws Exception {
        Assert.assertEquals("foo,bar", Joiner.join(Arrays.asList("foo", "bar"), ","));
        Assert.assertEquals("foo", Joiner.join(Arrays.asList("foo"), ","));
        Assert.assertEquals("foo", Joiner.join(Arrays.asList(null, "foo"), ","));
    }
}