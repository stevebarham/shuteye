* define exception strategy
* handle redirects
* check all public classes for appropriate equals, hashcode, tostring
* readme
* basic authentication
* javadoc over request, response, bufferedresponse


examples
========

* gets
* sending fields via posts 
* sending files via posts - encoding, streams, etc
* transforming responses - default transformer, specific character set
* output maven info to github
* update readme.md from maven 